from django.shortcuts import render


def index(request):
    html = render(request, 'cwp.html')
    return html
