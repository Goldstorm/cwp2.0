"""
WSGI config for cwp2 project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.0/howto/deployment/wsgi/
"""

import os
import sys

from django.core.wsgi import get_wsgi_application

#os.environ.setdefault("DJANGO_SETTINGS_MODULE", "cwp2.settings")
os.environ["DJANGO_SETTINGS_MODULE"] = "cwp2.settings"

sys.path.insert(0, '/opt/cwp2.0/')

application = get_wsgi_application()
